import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})



export class TodoListComponent {

  @Input() item = '';
  @Input() newTask = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();

  onDeleteTaskClick(){
    this.delete.emit();
  }

  onTaskInput(event: Event){
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }


}


