import {Component} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'TODO List';
  newTask: string = '';


  item = [
    {newTask: "Make some coffee"},
    {newTask: "Buy milk"},
    {newTask: "Grab food from store"}
  ];

  onTaskAdd(event: Event){
    event.preventDefault();
    this.item.push({
      newTask: this.newTask
    });
  }

  blockedFormInput(){
    return this.newTask === '';
  }

  onDeleteTaskClick(i: number){
    this.item.splice(i, 1);
  }

  changeTask(index: number, newTask: string){
    this.item[index].newTask = newTask;
  }


}
